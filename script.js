async function getIPInformation() {
    try {
        const ipInformationDiv = document.getElementById('ipInformation');
        ipInformationDiv.innerHTML = 'Шукаю тебе...';

        const ipifyResponse = await fetch('https://api.ipify.org/?format=json');
        const ipifyData = await ipifyResponse.json();
        const userIP = ipifyData.ip;
        

        const ipApiResponse = await fetch(`http://ip-api.com/json/${userIP}?fields=continent,country,regionName,city,district,query`);
        const ipApiData = await ipApiResponse.json();

        ipInformationDiv.innerHTML = `
            Континент: ${ipApiData.continent}<br>
            Країна: ${ipApiData.country}<br>
            Регіон: ${ipApiData.regionName}<br>
            Місто: ${ipApiData.city}<br>
            Район: ${ipApiData.district}<br>
            IP : ${userIP}<br>
        `;
    } catch (error) {
        console.error('Помилка при отриманні інформації:', error);
    }
}